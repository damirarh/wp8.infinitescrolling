﻿using System.Collections.ObjectModel;
using System.Windows.Input;
using Microsoft.Practices.Prism.Commands;

namespace WP8.InfiniteScrolling
{
    public class ViewModel
    {
        private const int BatchSize = 30;

        private readonly RemoteService _remoteService = new RemoteService(325);
        private bool _moreItemsAvailable = true;

        public ObservableCollection<string> Items { get; set; }
        public ICommand LoadCommand { get; set; }

        public ViewModel()
        {
            Items = new ObservableCollection<string>();
            Items.AddRange(_remoteService.LoadItems(0, BatchSize));

            LoadCommand = new DelegateCommand(LoadMoreItems, MoreItemsAvailable);
        }

        private bool MoreItemsAvailable()
        {
            return _moreItemsAvailable;
        }

        private void LoadMoreItems()
        {
            var expectedCount = Items.Count + BatchSize;
            Items.AddRange(_remoteService.LoadItems(Items.Count, BatchSize));
            _moreItemsAvailable = expectedCount == Items.Count;
        }
    }
}