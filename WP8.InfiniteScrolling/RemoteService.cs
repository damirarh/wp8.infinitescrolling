﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace WP8.InfiniteScrolling
{
    public class RemoteService
    {
        private readonly int _totalCount;

        public RemoteService(int totalCount)
        {
            _totalCount = totalCount;
        }

        public IEnumerable<string> LoadItems(int startIndex, int count)
        {
            Debug.WriteLine("Loading {0} items, starting at {1}", count, startIndex);
            int maxIndex = Math.Min(startIndex + count, _totalCount);
            for (int index = startIndex; index < maxIndex; index++)
            {
                yield return String.Format("Item {0}", index);
            }
        }
    }
}