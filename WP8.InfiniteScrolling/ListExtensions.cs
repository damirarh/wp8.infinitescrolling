﻿using System.Collections.Generic;

namespace WP8.InfiniteScrolling
{
    public static class ListExtensions
    {
        public static void AddRange<T>(this IList<T> list, IEnumerable<T> collection)
        {
            foreach (var item in collection)
            {
                list.Add(item);
            }
        }
    }
}